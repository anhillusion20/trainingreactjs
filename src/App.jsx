import React from "react";
import { useState } from "react";
// Component with useEffect call API
// import Page from "./pages/ComponentWithUseEffectCallAPI";

// Component with useEffect with preview image
// import Page from "./pages/ComponentWithUseEffectPreviewImage";

// Component with useReducer
import Page from "./pages/ComponentWithUseReducer";

const App = () => {
  const [toggle, setToggle] = useState(false);

  return (
    <>
      <div className="App">
        <button onClick={() => setToggle(!toggle)}>Toggle</button>
        {toggle && <Page />}
      </div>
    </>
  );
};

export default App;
