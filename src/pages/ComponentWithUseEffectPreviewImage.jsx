import React from "react";
import { useEffect, useState } from "react";

const ComponentWithUseEffectPreviewImage = () => {
  const [imageData, setImageData] = useState({
    preview: "",
    file: "",
  });

  useEffect(() => {
    // Callback
    if (!imageData.file) {
      return;
    }
    const imageURL = URL.createObjectURL(imageData.file);
    setImageData({
      ...imageData,
      preview: imageURL,
    });

    // Clean up
    return () => {
      URL.revokeObjectURL(imageURL);
    };
    // eslint-disable-next-line
  }, [imageData.file]);

  const handleChangeFile = (e) => {
    const files = e.target?.files;
    if (!files?.length) {
      return;
    }
    setImageData({
      ...imageData,
      file: files[0],
    });
  };

  return (
    <div>
      <input type="file" onChange={handleChangeFile} />
      {imageData.preview && <img src={imageData.preview} alt="" />}
    </div>
  );
};

export default ComponentWithUseEffectPreviewImage;
