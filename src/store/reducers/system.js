import { systemAction } from "../actions";

export const SYSTEM_INITIAL_STATE = {
  isLoading: false,
};

const reducer = (state, action) => {
  const { type, payload } = action;
  switch (type) {
    case systemAction.SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    default:
      throw new Error("Invalid System Action");
  }
};

export default reducer;
